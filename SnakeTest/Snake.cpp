#include <SDL.h>
#include <cstdlib>
#include <time.h>   
#include "SDL_mixer.h"
#include "SDL_image.h"
#include <SDL_ttf.h>
#include <sstream>
#include <string> 


using namespace std;



#define MUS_PATH "cartoon022.wav"
static const char *MP3 = "Alan.mp3";

const int tail_max = 255;

const int obstacles = 10;
int actualObstacleNum = 0;





struct Food {
	int x, y;

	void moveFood() {
		x = rand() % (400 / 16);
		y = rand() % (400 / 16);
	}

	void drawFood(SDL_Renderer* renderer) {
		SDL_Rect r{ 16 * x, 16 * y, 16, 16 };
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
		SDL_RenderFillRect(renderer, &r);
	}
};

struct Obstacle {
	int x, y;

	void moveObstacle() {
		x = rand() % (400 / 16);
		y = rand() % (400 / 16);
	}

	void drawObstacle(SDL_Renderer* renderer) {
		SDL_Rect r{ 16 * x, 16 * y, 16, 16 };
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
		SDL_RenderFillRect(renderer, &r);
	}
};

struct XYPosition {
	int x, y;
};

XYPosition operator+ (const XYPosition& a, const XYPosition& b) {
	return XYPosition{ a.x + b.x, a.y + b.y };
}

void operator+= (XYPosition& a, const XYPosition& b) {
	a.x += b.x;
	a.y += b.y;
}

struct Snake {
	XYPosition pos;
	XYPosition vel;
	XYPosition acc;

	XYPosition tail[tail_max];
	int tail_start, tail_end;
	int tail_len = 50;

	uint32_t accumulator;

	void update(uint32_t delta_time, struct Food& food) {
		accumulator += delta_time;
		if (accumulator > 100 - (tail_len)) {
			accumulator = 0;

			vel = acc;

			tail[tail_end % tail_max] = pos;

			tail_start++;
			tail_end++;

			pos.x += vel.x;
			pos.y += vel.y;

			pos.x = (pos.x + 25) % 25;
			pos.y = (pos.y + 25) % 25;

			// nowe jedzonko
			if (pos.x == food.x && pos.y == food.y) {
				tail_len += 1;
				tail_start -= 1;
				food.moveFood();
			}

			for (int i = 0; i < tail_len; i++) {
				XYPosition& tail_pos = tail[(tail_start + i) % tail_max];
				if (tail_pos.x == pos.x && tail_pos.y == pos.y) {
					//gdy wejdziemy na siebie
					tail_len = 0;
					tail_start = tail_end;

				}
			}
		}
	}

	void draw(SDL_Renderer* renderer) {
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

		//rw
		for (int i = 0; i < tail_len; i++) {
			XYPosition& tail_pos = tail[(tail_start + i) % tail_max];
			SDL_Rect r{ 16 * tail_pos.x, 16 * tail_pos.y, 16, 16 };
			SDL_RenderFillRect(renderer, &r);
		}

		SDL_Rect r{ 16 * pos.x, 16 * pos.y, 16, 16 };
		SDL_RenderFillRect(renderer, &r);
	}
};

int main(int argc, char** argv) {

	srand(time(NULL));

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window* window = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 400, 400, SDL_WINDOW_OPENGL);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_Event e;


		SDL_Surface  *screen;
		SDL_Surface *background = SDL_LoadBMP("Background.bmp");
		SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, background);


		bool running = true;
		Snake snake = {};
		Food food = {};
		food.moveFood();
		int maxScore = 0;

		TTF_Init();
		TTF_Font *font = NULL;
		SDL_Color textColor = { 255, 255, 255 };
		font = TTF_OpenFont("OpenSans-Bold.ttf", 28);
		if (font == NULL)
			return 0;
		SDL_Surface *message = NULL;
		SDL_Surface *scoreMessage = NULL;

		SDL_Rect Message_rect;
		Message_rect.x = 0; 
		Message_rect.y = 0; 
		Message_rect.w = 41;
		Message_rect.h = 41; 

		SDL_Rect MaxScore_rect;
		MaxScore_rect.x = 350;
		MaxScore_rect.y = 0;
		MaxScore_rect.w = 41;
		MaxScore_rect.h = 41;

		SDL_Event sdlEvent;

		SDL_GameController *controller = SDL_GameControllerOpen(0);

		for (int i = 0; i < SDL_NumJoysticks(); i++) {
			if (SDL_IsGameController(i)) {
				controller = SDL_GameControllerOpen(i);
				const char* name = SDL_GameControllerName(controller);
				printf("%s",
					name);
				break;
			}
		}


	uint32_t current_time = 0, previous_time, delta_time;

	while (running) {

		previous_time = current_time;
		current_time = SDL_GetTicks();
		delta_time = current_time - previous_time;

		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				running = false;
			}
			else if (e.type == SDL_CONTROLLERBUTTONDOWN) {
				switch (e.cbutton.button) {
				case SDLK_ESCAPE:
					running = false;
					break;

				case SDL_CONTROLLER_BUTTON_DPAD_UP: if (snake.vel.y != 1) snake.acc = { 0, -1 }; break;
				case SDL_CONTROLLER_BUTTON_DPAD_DOWN: if (snake.vel.y != -1) snake.acc = { 0, 1 }; break;
				case SDL_CONTROLLER_BUTTON_DPAD_LEFT: if (snake.vel.x != 1) snake.acc = { -1, 0 }; break;
				case SDL_CONTROLLER_BUTTON_DPAD_RIGHT: if (snake.vel.x != -1) snake.acc = { 1, 0 }; break;
				}
			}
			else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
				case SDLK_ESCAPE:
					running = false;
					break;

				case SDLK_w: if (snake.vel.y != 1) snake.acc = { 0, -1 }; break;
				case SDLK_s: if (snake.vel.y != -1) snake.acc = { 0, 1 }; break;
				case SDLK_a: if (snake.vel.x != 1) snake.acc = { -1, 0 }; break;
				case SDLK_d: if (snake.vel.x != -1) snake.acc = { 1, 0 }; break;
				}
			}
		}

		snake.update(delta_time, food);


		std::stringstream strm;
		strm << snake.tail_len;

		if (maxScore < snake.tail_len) {
			maxScore = snake.tail_len;
		}

		std::stringstream str;
		str << maxScore;

		SDL_FreeSurface(message);
		SDL_FreeSurface(scoreMessage);

		message = TTF_RenderText_Solid(font, strm.str().c_str(), textColor);
		SDL_Texture* score = SDL_CreateTextureFromSurface(renderer, message);

		scoreMessage = TTF_RenderText_Solid(font, str.str().c_str(), textColor);
		SDL_Texture* max = SDL_CreateTextureFromSurface(renderer, scoreMessage);



		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, NULL);

		SDL_RenderCopy(renderer, score, NULL, &Message_rect);
		SDL_RenderCopy(renderer, max, NULL, &MaxScore_rect);

		snake.draw(renderer);
		food.drawFood(renderer);
		


		SDL_RenderPresent(renderer);
	}






	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	TTF_CloseFont( font );
	TTF_Quit(); 

	SDL_Quit();

	return 0;


}
